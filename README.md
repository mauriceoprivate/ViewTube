# ViewTube

[![Build Status](https://travis-ci.org/mauriceoegerli/ViewTube.svg?branch=master)](https://travis-ci.org/mauriceoegerli/ViewTube)

Current Stage: Alpha

ViewTube is an alternative YouTube frontend using the [invidious](https://github.com/omarroth/invidious) API.

Unique features include:
- High Quality Playback up to 8k
- more coming soon
